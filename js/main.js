"use strict";
let canvas;

// function onMouseClick(event) {
//   console.log(event);
//   let settings = {
//     color: "pink",
//     location: { x: 200, y: 200 },
//   };
//   let disk = new Disk();
//   console.log(disk);
//   //   let radius = getRandomInteger();
//   disk.setColor(getRandomRGB());
//   disk.setRadius(getRandomInteger());
//   disk.setLocation(event.offsetX, event.offsetY);
//   disk.draw();
//   console.log(disk);
//   //   fillCircle(disk);
// }

function onMouseClick(event) {
  //console.log(event);
  //   let settings = {
  //     color: "pink",
  //     location: { x: 200, y: 200 },
  //   };
  let disk = new Disk(canvas);

  disk.setColor(getRandomRGBA());
  disk.setRadius(getRandomInteger(10, 50));
  disk.setLocation(event.offsetX, event.offsetY);

  disk.draw(canvas);

  console.log(disk);
  //   fillCircle(disk);
}

//function draw(e) {
// console.log("Allo");
//   var canvas = document.getElementById("masterpiece");
//   var ctx = canvas.getContext("2d");
//   var posx = e.clientX;
//   var posy = e.clientY;
//   var pos = getMousePos(canvas, e);
//   //ctx.fillStyle = "#00000";
//   //ctx.fillRect(pos.x - 25, pos.y - 25, 8, 8);
//   console.log("posx = " + pos.x + " ; posy = " + pos.y);
//   ctx.beginPath(); // arc de cercle vers le bas
//   ctx.lineWidth = "5";
//   ctx.strokeStyle = "#4C8"; // vert
//   ctx.arc(75, 100, 50, 0, Math.PI);
//   ctx.stroke();
//   ctx.beginPath();
//   ctx.lineWidth = "5";
//   ctx.strokeStyle = "#A4A";
//   ctx.arc(200, 100, 50, 0, 2 * Math.PI);
//   ctx.stroke();
//   ctx.beginPath();
//   ctx.lineWidth = "5";
//   ctx.fillStyle = "#48C";
//   ctx.arc(325, 100, 50, Math.PI, 2 * Math.PI);
//   ctx.fill();
//   ctx.beginPath();
//   ctx.lineWidth = "5";
//   ctx.fillStyle = "#48C";
//   ctx.arc(600, 200, 88, 0, 2 * Math.PI);
//   ctx.fill();
//   ctx.beginPath();
//   ctx.moveTo(50, 50);
//   ctx.lineTo(200, 200);
//   ctx.moveTo(200, 50);
//   ctx.lineTo(50, 200);
//   ctx.closePath();
//   ctx.stroke();
//}

// function getMousePos(canvas, evt) {
//   var rect = canvas.getBoundingClientRect();
//   return {
//     x: evt.clientX - rect.left,
//     y: evt.clientY - rect.top,
//   };
// }

// function disk() {}

/*----------------------------------------------------------------*/

// function fillCircle(disk) {
//   var canvas = CANVAS;
//   var ctx = canvas.getContext("2d");
//   //console.log("In function : " + disk);
//   var color = random_rgba();
//   ctx.beginPath();
//   ctx.fillStyle = color;
//   ctx.arc(disk.location.x, disk.location.y, disk.radius, 0, 2 * Math.PI);
//   ctx.fill();
// }
document.addEventListener("DOMContentLoaded", function () {
  canvas = document.querySelector("#masterpiece");
  canvas.addEventListener("click", onMouseClick);
});
