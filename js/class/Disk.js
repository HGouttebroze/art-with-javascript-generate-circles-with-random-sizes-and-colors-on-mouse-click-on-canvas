"use strict";

class Disk {
  color;
  radius;
  location;

  // methode private ms pas encore compatible et pas encore reconnu comme norme ds le language JS

  //#color;

  constructor(settings = {}) {
    const defaultValues = {
      color: "black",
      radius: 20,
      location: { x: 0, y: 0 },
    };
    for (let prop in this) {
      this[prop] = settings[prop] ? settings[prop] : defaultValues[prop];
    }
    console.log(this, settings);
  }

  setColor(color) {
    this.color = color;
  }

  setRadius(radius) {
    this.radius = radius;
  }

  setLocation(x, y) {
    this.location = { x, y };
  }

  draw(canvas) {
    console.log("faire dessiner le disk");
    const ctx = canvas.getContext("2d");

    ctx.beginPath();
    ctx.fillStyle = this.color;
    ctx.arc(this.location.x, this.location.y, this.radius, 0, 2 * Math.PI);
    ctx.fill();
  }
}
