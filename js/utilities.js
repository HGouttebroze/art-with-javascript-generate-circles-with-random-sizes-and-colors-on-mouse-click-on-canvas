"use strict"; // Mode strict du JavaScript

/*******************************************************************************************/
/* ******************************** FONCTIONS UTILITAIRES **********************************/
/*******************************************************************************************/

function getRandomInteger(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}
function getRandomRGBA() {
  let r = getRandomInteger(0, 255);
  let g = getRandomInteger(0, 255);
  let b = getRandomInteger(0, 255);
  let a = Math.random();
  return `rgba(${r}, ${g}, ${b}, ${a.toFixed(2)})`;
}
